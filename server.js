const app = require("./src/app");
const PORT = process.env.PORT || 3075;

app.listen(PORT, () => {
    console.log(`WSV Money Dating start with ${PORT} `)
})
