'use strict'
const { AuthFailureError, NotFoundError, BadRequestError } = require('../core/error.response');
const JWT = require('jsonwebtoken');
const asyncHandler = require('../helpers/asyncHandler')


//service
const { findByUserId } = require('../services/keytoken.service');

const HEADER = {
    API_KEY: 'x-api-key',
    CLIENT_ID: 'x-client-id',
    AUTHORIZATION: 'authorization',
    REFRESHTOKEN: 'x-rtoken-id'
}


const authentication = asyncHandler(async (req, res, next) => {

    const userId = req.headers[HEADER.CLIENT_ID]
    if (!userId) throw new BadRequestError('Invalid request');

    const keyStore = await findByUserId(userId);
    if (!keyStore) throw new NotFoundError('Not found keystore');

    if (req.headers[HEADER.REFRESHTOKEN]) {
        const refreshToken = req.headers[HEADER.REFRESHTOKEN]
        try {
            const decodeUser = JWT.verify(refreshToken, keyStore.privateKey);
            if (userId !== decodeUser.userId) throw new AuthFailureError('Invalid User')
            req.keyStore = keyStore
            req.user = decodeUser
            req.refreshToken = refreshToken
            return next()
        } catch (error) {
            throw new AuthFailureError('Invalid Token')
        }
    }

    const accessToken = req.headers[HEADER.AUTHORIZATION]
    if (!accessToken) throw new AuthFailureError('Invalid Request')
    try {
        const decodeUser = await JWT.verify(accessToken, keyStore.publicKey);
        if (userId !== decodeUser.userId) throw new AuthFailureError('Invalid User')
        req.keyStore = keyStore
        req.user = decodeUser //{userId, email}
        return next()
    } catch (error) {
        throw new AuthFailureError('Invalid Token')
    }
})

const verifyJWT = (token, keySecret) => {
    try {
        const decodeUser = JWT.verify(token, keySecret)
        return decodeUser;
    } catch (error) {
        throw new AuthFailureError(`JWT verification failed: ${error.message}`)
    }
}

const createTokenPair = async (payload, publicKey, privateKey) => {
    try {
        const accessToken = await JWT.sign(payload, publicKey, {
            expiresIn: '1h'
        })
        const refreshToken = await JWT.sign(payload, privateKey, {
            expiresIn: '1d'
        })

        JWT.verify(accessToken, publicKey, (err, decode) => {
            if (err) {
                console.log(`error verify::`, err)
            } else {
                console.log(`decode success::`, decode);
            }
        })

        return { accessToken, refreshToken }
    } catch (error) {
        console.log(error);
    }
}

module.exports = {
    createTokenPair,
    verifyJWT,
    authentication,
}