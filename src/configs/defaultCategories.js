module.exports = [
    {
        name: 'Food',
        type: 'expense',
        icon_dir: 'assets/category-icon/food.svg'
    },
    {
        name: 'Bills',
        type: 'expense',
        icon_dir: 'assets/category-icon/bills.svg'
    },
    {
        name: 'Transportation',
        type: 'expense',
        icon_dir: 'assets/category-icon/transportation.svg'
    },
    {
        name: 'Family',
        type: 'expense',
        icon_dir: 'assets/category-icon/family.svg'
    },
    {
        name: 'Shopping',
        type: 'expense',
        icon_dir: 'assets/category-icon/shopping.svg'
    },
    {
        name: 'Party',
        type: 'expense',
        icon_dir: 'assets/category-icon/party.svg'
    },
    {
        name: 'Restaurant',
        type: 'expense',
        icon_dir: 'assets/category-icon/restaurant.svg'
    },
    {
        name: 'Coffee',
        type: 'expense',
        icon_dir: 'assets/category-icon/coffee.svg'
    },
    {
        name: 'Lover',
        type: 'expense',
        icon_dir: 'assets/category-icon/broken-heart.svg'
    },
    {
        name: 'Beer',
        type: 'expense',
        icon_dir: 'assets/category-icon/beer.svg'
    },
    {
        name: 'Entertainment',
        type: 'expense',
        icon_dir: 'assets/category-icon/entertainment.svg'
    },
    {
        name: 'Health',
        type: 'expense',
        icon_dir: 'assets/category-icon/health.svg'
    },
    {
        name: 'Education',
        type: 'expense',
        icon_dir: 'assets/category-icon/education.svg'
    },
    {
        name: 'Rental',
        type: 'expense',
        icon_dir: 'assets/category-icon/rental.svg'
    },
    {
        name: 'Movie',
        type: 'expense',
        icon_dir: 'assets/category-icon/movie.svg'
    },
    {
        name: 'Game',
        type: 'expense',
        icon_dir: 'assets/category-icon/game.svg'
    },
    {
        name: 'Television',
        type: 'expense',
        icon_dir: 'assets/category-icon/television.svg'
    },
    {
        name: 'Donation',
        type: 'expense',
        icon_dir: 'assets/category-icon/donation.svg'
    },
    {
        name: 'Investment',
        type: 'expense',
        icon_dir: 'assets/category-icon/investment.svg'
    },
    {
        name: 'Saving',
        type: 'expense',
        icon_dir: 'assets/category-icon/saving.svg'
    },
    {
        name: 'Other Expense',
        type: 'expense',
        icon_dir: 'assets/category-icon/other-expense.svg'
    },
    {
        name: 'Salary',
        type: 'income',
        icon_dir: 'assets/category-icon/salary.svg'
    },
    {
        name: 'Collect Interest',
        type: 'income',
        icon_dir: 'assets/category-icon/collect-interest.svg'
    },
    {
        name: 'Selling',
        type: 'income',
        icon_dir: 'assets/category-icon/selling.svg'
    },
    {
        name: 'Award',
        type: 'income',
        icon_dir: 'assets/category-icon/award.svg'
    },
    {
        name: 'Other Income',
        type: 'income',
        icon_dir: 'assets/category-icon/other-income.svg'
    },
    {
        name: 'Loan',
        type: 'expense',
        icon_dir: 'assets/category-icon/loan.svg',
        is_owe_type: true
    },
    {
        name: 'Repayment',
        type: 'expense',
        icon_dir: 'assets/category-icon/repayment.svg',
        is_owe_type: true
    },
    {
        name: 'Debt',
        type: 'income',
        icon_dir: 'assets/category-icon/debt.svg',
        is_owe_type: true
    },
    {
        name: 'Debt Collection',
        type: 'income',
        icon_dir: 'assets/category-icon/debt-collection.svg',
        is_owe_type: true
    }
]