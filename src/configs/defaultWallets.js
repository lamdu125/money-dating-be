module.exports = [
    {
        name: 'Cash',
        type: 'basic',
        currency: 'USD',
        include_in_total: true,
        balance: 0,
        initial_balance: 0,
    },
    {
        name: 'Credit',
        type: 'credit',
        currency: 'VND',
        include_in_total: false,
        balance: 0,
        initial_balance: 45000000,
        attributes: {
            credit_limit: 45000000,
            statement_day: 10,
            payment_due_day: 25,
        }
    },
    {
        name: 'Wedding',
        type: 'goal',
        currency: 'USD',
        include_in_total: false,
        balance: 0,
        initial_balance: 0,
        attributes: {
            goal_value: 100000000,
            ending_date: '12/31/2024'
        }
    }
]