'use strict'

const AccessService = require("../services/access.service")
const { CREATED, SuccessResponse } = require("../core/success.response")

class AccessController {
    signUp = async (req, res, next) => {
        new CREATED({
            message: 'Register success',
            metadata: await AccessService.signUp(req.body)
        }).send(res)
    }
    login = async (req, res, next) => {
        new SuccessResponse({
            message: 'Login success',
            metadata: await AccessService.login(req.body)
        }).send(res)
    }
    logout = async (req, res, next) => {
        new SuccessResponse({
            message: "Logout success",
            metadata: await AccessService.logout(req.keyStore)
        }).send(res)
    }
    handlerAccessToken = async (req, res, next) => {
        new SuccessResponse({
            message: "Authorization success",
            metadata: AccessService.handlerAccessToken(req.user)
        }).send(res)
    }
    handlerRefreshToken = async (req, res, next) => {
        new SuccessResponse({
            message: 'Refresh token success',
            metadata: await AccessService.handlerRefreshToken({
                keyStore: req.keyStore,
                user: req.user,
                refreshToken: req.refreshToken
            })
        }).send(res)
    }
    loginFacebook = async (req, res, next) => {
        new SuccessResponse({
            message: 'Login fb success',
            metadata: await AccessService.logInWithAuthProvider({
                authProvider: 'facebook',
                token: req.body.accessToken
            })
        }).send(res)
    }
    loginGoogle = async (req, res, next) => {
        new SuccessResponse({
            message: 'Login google success',
            metadata: await AccessService.logInWithAuthProvider({
                authProvider: 'google',
                token: req.body.accessToken
            })
        }).send(res)
    }
    forgotPassword = async (req, res, next) => {
        new SuccessResponse({
            message: 'Send mail success',
            metadata: await AccessService.forgotPassword(req.body)
        }).send(res);
    }
    resetPassword = async (req, res, next) => {
        new SuccessResponse({
            message: 'reset password success',
            metadata: await AccessService.resetPassword({
                ...req.body,
                repeatPassword: req.body.repeat_password
            })
        }).send(res);
    }
}

module.exports = new AccessController()