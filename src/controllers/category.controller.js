'use strict'

const { CREATED, SuccessResponse } = require("../core/success.response")
const CategoryService = require("../services/category.service")

class CategoryController {
    listCategories = async (req, res, next) => {
        new SuccessResponse({
            message: 'get list categories success',
            metadata: await CategoryService.getCustomCategories({
                userId: req.user.userId,
                type: req.params.type
            })
        }).send(res)
    }
    updateCategory = async (req, res, next) => {
        new SuccessResponse({
            message: 'update cat successfully',
            metadata: await CategoryService.updateCategory({
                userId: req.user.userId,
                catId: req.params.catId,
                payload: req.body
            })
        }).send(res)
    }
    createCategory = async (req, res, next) => {
        new CREATED({
            message: 'add category successfully',
            metadata: await CategoryService.createCategory({
                userId: req.user.userId,
                payload: req.body
            })
        }).send(res)
    }
    deleteCategory = async (req, res, next) => {
        new SuccessResponse({
            message: 'deleted category',
            metadata: await CategoryService.deleteCategory({
                userId: req.user.userId,
                catId: req.params.catId,
                replaceId: req.params.replaceId
            })
        }).send(res)
    }
}

module.exports = new CategoryController()