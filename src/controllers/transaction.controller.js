'use strict'

const { CREATED, SuccessResponse } = require("../core/success.response")
const TransactionService = require("../services/transaction.service")

class TransactionController {
    createTransaction = async (req, res, next) => {
        new CREATED({
            message: 'Create Transaction Success',
            metadata: await TransactionService.create({
                userId: req.user.userId,
                payload: req.body
            })
        }).send(res)
    }
    getListTransactions = async (req, res, next) => {
        new SuccessResponse({
            message: 'get list success',
            metadata: await TransactionService.getListTransactions({
                userId: req.user.userId,
                query: req.body
            })
        }).send(res)
    }
    deleteTransactions = async (req, res, next) => {
        new SuccessResponse({
            message: 'delete trans success',
            metadata: await TransactionService.deleteTransactions({
                transactionIds: req.body.ids
            })
        }).send(res)
    }
    updateTransaction = async (req, res, next) => {
        new SuccessResponse({
            message: 'update wallet success',
            metadata: await TransactionService.updateTransaction({
                userId: req.user.userId,
                transactionId: req.params.transactionId,
                payload: req.body
            })
        }).send(res)
    }
    getWalletStatic = async (req, res, next) => {
        new SuccessResponse({
            message: 'Get wallet static success',
            metadata: await TransactionService.getWalletStatic({
                userId: req.user.userId,
                walletId: req.body.walletId,
                startDate: req.body.startDate,
                endDate: req.body.endDate
            })
        }).send(res)
    }
    getTransactionStatic = async (req, res, next) => {
        new SuccessResponse({
            message: 'Get transaction static success',
            metadata: await TransactionService.getTransactionsStaticPerDay({
                userId: req.user.userId,
                walletId: req.body.walletId,
                startDate: req.body.startDate,
                endDate: req.body.endDate
            })
        }).send(res)
    }
    getExpenseCategoryStatic = async (req, res, next) => {
        new SuccessResponse({
            message: 'Get transaction static success',
            metadata: await TransactionService.getExpenseCategoryStatic({
                userId: req.user.userId,
                walletId: req.body.walletId,
                startDate: req.body.startDate,
                endDate: req.body.endDate
            })
        }).send(res)
    }
    exportCSV = async (req, res, next) => {
        const csvData = await TransactionService.exportCSV({
            userId: req.user.userId,
            query: req.body
        });
        res.header('Content-Type', 'text/csv');
        res.attachment('transactions.csv');
        res.send(csvData);
    }
}

module.exports = new TransactionController()