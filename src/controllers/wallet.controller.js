'use strict'

const { CREATED, SuccessResponse } = require("../core/success.response")
const walletService = require('../services/wallet.service')

class WalletController {
    listWallets = async (req, res, next) => {
        new SuccessResponse({
            message: 'get list wallets success',
            metadata: await walletService.getCustomWallets({ userId: req.user.userId })
        }).send(res)
    }
    updateWallet = async (req, res, next) => {
        new SuccessResponse({
            message: 'update wallet success',
            metadata: await walletService.updateWallet({
                userId: req.user.userId,
                walletId: req.params.walletId,
                payload: req.body
            })
        }).send(res)
    }
    createWallet = async (req, res, next) => {
        new CREATED({
            message: 'add Wallet successfully',
            metadata: await walletService.createWallet({
                userId: req.user.userId,
                payload: req.body
            })
        }).send(res)
    }
    deleteWallet = async (req, res, next) => {
        new SuccessResponse({
            message: 'delete wallet success',
            metadata: await walletService.deleteWallet({
                userId: req.user.userId,
                walletId: req.params.walletId
            })
        }).send(res)
    }
}

module.exports = new WalletController()