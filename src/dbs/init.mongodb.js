'use strict'
const mongoose = require('mongoose');
const { db: { host, name, port } } = require('../configs/config.mongdb')
const { countConnect } = require('../helpers/check.connect');

const connectString = process.env.MONGO_CONNECT_STRING == "" ? `mongodb://${host}:${port}/${name}` : process.env.MONGO_CONNECT_STRING

class Database {
    constructor() {
        this.connect()
    }

    connect(type = 'mongodb') {

        if (1 === 1) {
            mongoose.set('debug', true);
            mongoose.set('debug', { color: true })
        }

        mongoose.connect(connectString).then(_ => {
            console.log(`Connected Mongodb Success`, countConnect());

        })
            .catch(err => console.log(`Error Connect!`));
    }

    static getInstance() {
        if (!Database.instance) {
            Database.instance = new Database()
        }

        return Database.instance
    }
}

const instanceMongodb = Database.getInstance()

module.exports = instanceMongodb