'use strict'

const mongoose = require('mongoose')
const os = require('os');
const process = require('process')

const _SECONDS = 5000

const countConnect = () => {
    const numConnection = mongoose.connections.length;
    console.log(`Number of connection: ${numConnection}`)
}

const checkOverload = () => {
    setInterval(() => {
        const numConnections = mongoose.connections.length;
        const numCores = os.cpus().length;
        const memoryUsage = process.memoryUsage().rss
        maxConnections = numCores * 5;

        console.log(`Active connnections:: ${numConnections}`)
        console.log(`Memory usage:: ${memoryUsage / 1024 / 1024} MB`)

        if (numConnection > maxConnections) {
            console.log(`Connections overload detected`)
        }
    }, _SECONDS)
}

module.exports = {
    countConnect
}