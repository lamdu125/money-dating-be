const { NotFoundError } = require('../core/error.response');

const fs = require('fs').promises

const readHtmlFile = async (path) => {
    try {
        const htmlContent = await fs.readFile(path, 'utf-8')
        return htmlContent
    } catch (error) {
        console.error('Error reading the HTML file::', error);
        throw new NotFoundError('Not found template file');
    }
}

module.exports = {
    readHtmlFile
}