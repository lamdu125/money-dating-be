const { BadRequestError } = require("../core/error.response");

const convertToFutureDate = (str) => {
    const match = str.match(/^(\d+)(h|d|m)$/);
    if (!match) {
        throw new BadRequestError('invalid format');
    }
    const [, value, unit] = match;
    const numericValue = parseInt(value, 10)
    const now = new Date();

    switch (unit) {
        case 'h':
            now.setHours(now.getHours() + numericValue);
            break
        case 'd':
            now.setDate(now.getDate() + numericValue);
            break;
        case 'm':
            now.setMinutes(now.getMinutes() + numericValue);
            break;
        default:
            throw new BadRequestError('invalid unit');
    }

    return now;
}
module.exports = {
    convertToFutureDate
}
