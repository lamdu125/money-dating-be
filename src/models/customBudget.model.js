'use strict'

const { Schema } = require('mongoose'); // Erase if already required

// Declare the Schema of the Mongo model
var customBudgetSchema = new Schema({
    category_id: {
        type: Schema.Types.ObjectId,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
    },
    value: {
        type: Number,
        required: true
    },
    startDate: {
        type: Date,
        required: true
    },
    endDate: {
        type: Date,
        required: true
    }
});

//Export the model
module.exports = customBudgetSchema