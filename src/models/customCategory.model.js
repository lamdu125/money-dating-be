'use strict'

const { Schema } = require('mongoose'); // Erase if already required


// Declare the Schema of the Mongo model
var customCategorySchema = new Schema({
    parent_category: {
        type: String
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
    },
    type: {
        type: String,
        enum: ['income', 'expense'],
        default: 'expense'
    },
    icon_dir: {
        type: String,
    },
    is_owe_type: {
        type: Boolean,
        default: false
    }
});

//Export the model
module.exports = customCategorySchema
