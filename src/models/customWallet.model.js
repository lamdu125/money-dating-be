'use strict'

const { Schema } = require('mongoose'); // Erase if already required

// Declare the Schema of the Mongo model
var walletSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    currency: {
        type: String,
        required: true
    },
    include_in_total: {
        type: Boolean,
        required: true
    },
    balance: {
        type: Number,
        required: true
    },
    initial_balance: {
        type: Number,
        required: true
    },
    attributes: {
        type: Schema.Types.Mixed
    }
});

//Export the model
module.exports = walletSchema;