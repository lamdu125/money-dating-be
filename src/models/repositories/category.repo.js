'use strict'

const { NotFoundError, BadRequestError } = require("../../core/error.response");
const { findUserById } = require("../../services/user.service");
const { convertToObjectIdMongodb } = require('../../utils')
const userModel = require("../user.model")


const getCategoryById = async (userId, catId) => {
    const catList = await getCustomCategoriesByUserId(userId);
    const foundCat = catList.find(cat => cat._id.toString() === catId)
    return foundCat
}

const getCustomCategoriesByUserId = async (userId) => {
    const user = await userModel.findById(userId).populate('categories').exec();
    if (!user) throw new NotFoundError('Not found user');
    return user.categories;
}

const getListCatByType = async (userId, type) => {
    const catList = await getCustomCategoriesByUserId(userId);
    const listByType = catList.filter((catItem) => {
        if (type === 'owe') return catItem['is_owe_type'] === true
        return !catItem['is_owe_type'] && catItem['type'] === type
    })
    return listByType
}

const updateCategoryById = async (userId, catId, payload) => {
    const user = await findUserById(userId)

    const categoryIndex = user.categories.findIndex(c => c._id.toString() === catId);
    if (categoryIndex === -1) {
        throw new NotFoundError('Not found category')
    }

    user.categories[categoryIndex] = {
        ...user.categories[categoryIndex].toObject(),
        ...payload
    }
    if (user.categories[categoryIndex].parent_category) {
        const parentId = user.categories[categoryIndex].parent_category
        user.categories.forEach(catItem => {
            if (catItem.parent_category === catId) {
                catItem.parent_category = parentId
            }
        })
    }

    await user.save()
    return user.categories[categoryIndex]
}


const createCategory = async (userId, payload) => {
    if (!payload.name || !payload.icon_dir) throw new BadRequestError('Empty fields')

    const user = await findUserById(userId);
    const foundCat = user.categories.some((category) => category.name === payload.name)
    if (foundCat) throw new BadRequestError('Category name is existed');

    user.categories.push({
        ...payload
    })

    await user.save()
    return user.categories[user.categories.length - 1]
}

const deleteCategoryById = async (userId, catId) => {
    if (!catId) throw new BadRequestError('Invalid field')
    const result = await userModel.updateOne(
        { '_id': convertToObjectIdMongodb(userId) },
        { $pull: { "categories": { '_id': convertToObjectIdMongodb(catId) } } }
    )
    if (result.modifiedCount === 0) throw new NotFoundError('Category not found or delete failed');
    return result
}

module.exports = {
    getCustomCategoriesByUserId,
    updateCategoryById,
    createCategory,
    deleteCategoryById,
    getListCatByType,
    getCategoryById
}