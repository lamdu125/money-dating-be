'use strict'

const { isNumber } = require("lodash");
const { NotFoundError, BadRequestError, ForbiddenError } = require("../../core/error.response");
const Transaction = require("../transaction.model")
const { getCategoryById } = require("./category.repo");
const { getWalletById, getWalletsInTotal } = require("./wallet.repo");
const { getSelectData, convertToObjectIdMongodb } = require("../../utils");


const getTransactionById = async (id) => {
    return await Transaction.findById(id).lean();;
}

const updateTransactionById = async (transactionsId, payload) => {
    const updatedTransaction = await Transaction.findByIdAndUpdate(
        transactionsId,
        payload,
        { new: true })
        .lean();
    if (!updatedTransaction) throw new NotFoundError('Not found transaction');
    const { _id, category_id, wallet_id, amount, note, date } = updatedTransaction
    return { _id, category_id, wallet_id, amount, note, date };
}

const createTransaction = async (userId, payload) => {
    const foundCat = await getCategoryById(userId, payload.category_id);
    if (!foundCat) throw new NotFoundError('Not found category');

    const foundWallet = await getWalletById(userId, payload.wallet_id);
    if (!foundWallet) throw new NotFoundError('Not found wallet');

    const date = new Date(payload.date);
    if (isNaN(date.getTime())) throw new BadRequestError('The date not valid');

    if (!isNumber(payload.amount)) throw new BadRequestError('Not valid amount')

    const newTransaction = await Transaction.create({ ...payload, user_id: userId, date: date });
    if (!newTransaction) throw new Error('Some thing wrong');

    return newTransaction;
}

const getTransactions = async ({
    userId,
    page = 1,
    limit = 10,
    filters,
    sortField = 'date',
    sortDirection = 'desc',
    select = ['wallet_id', 'category_id', 'date', '_id', 'note', 'amount']
}) => {
    const query = { user_id: userId };

    if (filters.date && filters.date.start && !filters.date.end) {
        query.date = { $gte: new Date(filters.date.start) };
    }
    if (filters.date && !filters.date.start && filters.date.end) {
        query.date = { $lte: new Date(filters.date.end) };
    }
    if (filters.date && filters.date.start && filters.date.end) {
        query.date = { $gte: new Date(filters.date.start), $lte: new Date(filters.date.end) };
    }
    if (filters.category_id) {
        if (filters.category_id === -1) {
            query.amount = { $lte: 0 }
        } else if (filters.category_id === 1) {
            query.amount = { $gte: 0 }
        } else {
            query.category_id = filters.category_id;
        }
    }
    if (filters.wallet_id) {
        query.wallet_id = filters.wallet_id;
    }

    const sortOptions = {};
    sortOptions[sortField] = sortDirection === 'asc' ? 1 : -1;

    const transactions = await Transaction.find(query)
        .sort(sortOptions)
        .skip((page - 1) * limit)
        .limit(limit)
        .select(getSelectData(select))
        .lean();

    const totalCount = await Transaction.countDocuments(query);

    return {
        transactions,
        totalCount,
        totalPages: Math.ceil(totalCount / limit),
        currentPage: page
    };
};
const getTransactionsExport = async ({
    userId,
    filters,
    sortField = 'date',
    sortDirection = 'desc',
    select = ['wallet_id', 'category_id', 'date', 'note', 'amount']
}) => {
    const query = { user_id: userId };

    if (filters.date && filters.date.start && !filters.date.end) {
        query.date = { $gte: new Date(filters.date.start) };
    }
    if (filters.date && !filters.date.start && filters.date.end) {
        query.date = { $lte: new Date(filters.date.end) };
    }
    if (filters.date && filters.date.start && filters.date.end) {
        query.date = { $gte: new Date(filters.date.start), $lte: new Date(filters.date.end) };
    }
    if (filters.category_id) {
        if (filters.category_id === -1) {
            query.amount = { $lte: 0 }
        } else if (filters.category_id === 1) {
            query.amount = { $gte: 0 }
        } else {
            query.category_id = filters.category_id;
        }
    }
    if (filters.wallet_id) {
        query.wallet_id = filters.wallet_id;
    }

    const sortOptions = {};
    sortOptions[sortField] = sortDirection === 'asc' ? 1 : -1;

    return await Transaction.find(query)
        .sort(sortOptions)
        .select(getSelectData(select))
        .lean();

};

const deleteTransactionsByIds = async (transactionIds) => {
    const result = await Transaction.deleteMany({ _id: { $in: transactionIds } });
    if (!result) throw new Error('fail to delete');
    return true;
}

const getTransactionByWallet = async (userId, walletId) => {
    return await Transaction.find({ user_id: userId, wallet_id: walletId }).lean();
}

const getTransactionByCategory = async (userId, catId) => {
    return await Transaction.find({ user_id: userId, category_id: catId }).lean();
}

const getTransactionsByTimePeriod = async (userId, walletId = null, startDate, endDate) => {
    const query = {
        user_id: convertToObjectIdMongodb(userId),
    }
    if (startDate || endDate) {
        query.date = {};
        if (startDate) {
            query.date.$gte = new Date(startDate);
        }
        if (endDate) {
            query.date.$lte = new Date(endDate);
        }
    }
    if (walletId) {
        query.wallet_id = walletId
    } else {
        const walletInTotal = await getWalletsInTotal(userId);
        const walletInTotalId = walletInTotal.map(wallet => wallet._id.toString());
        query.wallet_id = { $in: walletInTotalId };
    }
    return await Transaction.find(query);
}

const getTransactionsStaticPerDay = async (userId, walletId = null, startDate, endDate) => {
    const query = {
        user_id: convertToObjectIdMongodb(userId),
    }
    if (startDate || endDate) {
        query.date = {};
        if (startDate) {
            query.date.$gte = new Date(startDate);
        }
        if (endDate) {
            query.date.$lte = new Date(endDate);
        }
    }
    if (walletId) {
        query.wallet_id = walletId
    } else {
        const walletInTotal = await getWalletsInTotal(userId);
        const walletInTotalId = walletInTotal.map(wallet => wallet._id.toString());
        query.wallet_id = { $in: walletInTotalId };
    }

    const pipeline = [
        {
            $match: query
        },
        {
            $group: {
                _id: { $dateToString: { format: "%Y-%m-%d", date: "$date" } },
                income: { $sum: { $cond: [{ $gt: ["$amount", 0] }, "$amount", 0] } },
                expense: { $sum: { $cond: [{ $lt: ["$amount", 0] }, { $abs: "$amount" }, 0] } }
            }
        },
        {
            $project: {
                _id: 0,
                date: "$_id",
                income: 1,
                expense: 1
            }
        },
        {
            $sort: { date: 1 }
        }
    ];

    return await Transaction.aggregate(pipeline);
}
const getExpenseCategoryStatic = async (userId, walletId = null, startDate, endDate) => {
    const query = {
        user_id: convertToObjectIdMongodb(userId),
        amount: { $lte: 0 }
    }
    if (startDate || endDate) {
        query.date = {};
        if (startDate) {
            query.date.$gte = new Date(startDate);
        }
        if (endDate) {
            query.date.$lte = new Date(endDate);
        }
    }
    if (walletId) {
        query.wallet_id = walletId
    } else {
        const walletInTotal = await getWalletsInTotal(userId);
        const walletInTotalId = walletInTotal.map(wallet => wallet._id.toString());
        query.wallet_id = { $in: walletInTotalId };
    }

    const pipeline = [
        {
            $match: query
        },
        {
            $group: {
                _id: "$category_id",
                totalAmount: { $sum: "$amount" }
            }
        },
        {
            $sort: { totalAmount: 1 }
        },
        {
            $facet: {
                topCategories: [
                    { $limit: 5 },
                    {
                        $project: {
                            _id: 0,
                            name: "$_id",
                            value: "$totalAmount"
                        }
                    }
                ],
                otherCategories: [
                    { $skip: 5 },
                    {
                        $group: {
                            _id: null,
                            totalAmount: { $sum: "$totalAmount" }
                        }
                    },
                    {
                        $project: {
                            _id: 0,
                            name: "Others",
                            value: "$totalAmount"
                        }
                    }
                ]
            }
        },
        {
            $project: {
                result: {
                    $concatArrays: ["$topCategories", "$otherCategories"]
                }
            }
        },
        {
            $unwind: "$result"
        },
        {
            $replaceRoot: { newRoot: "$result" }
        }
    ];

    return await Transaction.aggregate(pipeline);
}

module.exports = {
    createTransaction,
    getTransactions,
    deleteTransactionsByIds,
    getTransactionById,
    updateTransactionById,
    getTransactionByWallet,
    getTransactionByCategory,
    getTransactionsByTimePeriod,
    getTransactionsStaticPerDay,
    getExpenseCategoryStatic,
    getTransactionsExport
}