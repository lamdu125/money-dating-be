'use strict'

const { NotFoundError, BadRequestError } = require("../../core/error.response");
const { findUserById } = require("../../services/user.service");
const { convertToObjectIdMongodb } = require('../../utils')
const userModel = require("../user.model")


const getWalletById = async (userId, walletId) => {
    const walletList = await getWalletsByUserId(userId);
    return walletList.filter(wallet => wallet._id.toString() === walletId);
}

const getWalletsInTotal = async (userId) => {
    const allWallet = await getWalletsByUserId(userId);
    return allWallet.filter(wallet => wallet.include_in_total === true);
}

const getWalletsByUserId = async (userId) => {
    const user = await userModel.findById(userId).populate('wallets').exec();
    if (!user) throw new NotFoundError('Not found user');
    return user.wallets;
}

const createWallet = async (userId, payload) => {
    const user = await findUserById(userId)
    const foundWallet = user.wallets.some(wallet => wallet.name === payload.name)
    if (foundWallet) throw new BadRequestError('Wallet name is existed')
    user.wallets.push(payload)
    await user.save()
    return user.wallets[user.wallets.length - 1]
}
const updateWalletById = async (userId, walletId, updateBody) => {
    const update = {};
    for (const key in updateBody) {
        update[`wallets.$.${key}`] = updateBody[key];
    }
    const updatedUser = await userModel.findOneAndUpdate(
        {
            _id: userId,
            'wallets._id': convertToObjectIdMongodb(walletId)
        },
        {
            $set: update
        },
        { new: true }
    );
    return updatedUser.wallets;
}
const updateWalletBalance = async (userId, walletId, amount) => {
    const updatedUser = await userModel.findOneAndUpdate(
        {
            _id: userId,
            'wallets._id': convertToObjectIdMongodb(walletId)
        },
        {
            $inc: { 'wallets.$.balance': amount }
        },
        {
            new: true
        }
    )
    if (!updatedUser) return false
    return true
}

const revertWalletBalance = async (userId, walletId, amount) => {
    const updatedUser = await userModel.findOneAndUpdate(
        {
            _id: userId,
            'wallets._id': convertToObjectIdMongodb(walletId)
        },
        {
            $inc: { 'wallets.$.balance': -amount }
        },
        {
            new: true
        }
    )
    if (!updatedUser) throw new Error('Not found wallet or update error');
    return true;
}

const deleteWalletById = async (userId, walletId) => {
    if (!walletId) throw new BadRequestError('Invalid field')
    const result = await userModel.updateOne(
        { '_id': convertToObjectIdMongodb(userId) },
        { $pull: { "wallets": { '_id': convertToObjectIdMongodb(walletId) } } }
    )
    if (result.modifiedCount === 0) throw new NotFoundError('Wallet not found or delete failed');

    return result
}
module.exports = {
    getWalletsByUserId,
    createWallet,
    updateWalletById,
    deleteWalletById,
    getWalletById,
    updateWalletBalance,
    revertWalletBalance,
    getWalletsInTotal
}