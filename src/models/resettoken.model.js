'use strict'

const { model, Schema } = require('mongoose');

const DOCUMENT_NAME = 'Reset-token'
const COLLECTION_NAME = 'Reset-tokens'

var resetTokenSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    token: {
        type: String,
        required: true
    },
    expiresAt: {
        type: Date,
        required: true,
        index: { expires: '0' }
    }
}, {
    collection: COLLECTION_NAME,
    timestamps: true
})

module.exports = model(DOCUMENT_NAME, resetTokenSchema)