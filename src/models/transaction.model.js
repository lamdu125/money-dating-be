'use strict'

const { model, Schema, Types } = require('mongoose'); // Erase if already required
const { updateWalletBalance, revertWalletBalance } = require('./repositories/wallet.repo');
const { NotFoundError } = require('../core/error.response');

const DOCUMENT_NAME = 'Transaction'
const COLLECTION_NAME = 'Transactions'

// Declare the Schema of the Mongo model
var transactionSchema = new Schema({
    user_id: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    date: {
        type: Date,
        required: true
    },
    category_id: {
        type: String,
        required: true
    },
    wallet_id: {
        type: String,
        required: true,
    },
    amount: {
        type: Number,
        required: true,
    },
    note: {
        type: String,
        default: ''
    }
}, {
    timestamps: true,
    collection: COLLECTION_NAME
});


transactionSchema.index({ date: 1 });
transactionSchema.index({ user_id: 1 })


transactionSchema.index({ user_id: 1, date: 1, category_id: 1 });
transactionSchema.index({ user_id: 1, date: 1, wallet_id: 1 });

transactionSchema.post('save', async function (doc, next) {
    const result = await updateWalletBalance(doc.user_id, doc.wallet_id, doc.amount)
    if (!result) throw new NotFoundError('Fail to update wallet');
    next();
})
transactionSchema.post('findOneAndUpdate', async function (doc, next) {
    const result = await updateWalletBalance(doc.user_id, doc.wallet_id, doc.amount)
    if (!result) throw new NotFoundError('Fail to update wallet');
    next();
})
transactionSchema.pre('deleteMany', async function (next) {
    const filter = this.getFilter();

    const transactions = await this.model.find(filter);
    for (const transaction of transactions) {
        const result = await revertWalletBalance(transaction.user_id, transaction.wallet_id, transaction.amount);
        if (!result) throw new Error('Fail to update wallet');
    }
    next();
})
transactionSchema.pre('findOneAndUpdate', async function (next) {
    const filter = this.getFilter();

    const transaction = await this.model.findOne(filter);
    if (transaction) {
        const result = await revertWalletBalance(transaction.user_id, transaction.wallet_id, transaction.amount);
        if (!result) throw new Error('Fail to update wallet');
    }
    next();
})
//Export the model
const Transaction = model(DOCUMENT_NAME, transactionSchema);

Transaction.createIndexes()


module.exports = Transaction;
