'use strict'

const { model, Schema, Types } = require('mongoose'); // Erase if already required

const CustomCategories = require('./customCategory.model')
const CustomWallets = require('./customWallet.model')
const CustomBudgets = require('./customBudget.model')

const DOCUMENT_NAME = 'User'
const COLLECTION_NAME = 'Users'

// Declare the Schema of the Mongo model
var userSchema = new Schema({
    name: {
        type: String,
        trim: true,
        maxLength: 150
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String
    },
    verify: {
        type: Schema.Types.Boolean,
        default: false
    },
    roles: {
        type: Array,
        default: []
    },
    authProvider: {
        type: String,
    },
    categories: [CustomCategories],
    wallets: [CustomWallets],
    budgets: [CustomBudgets]
}, {
    timestamps: true,
    collection: COLLECTION_NAME
});

//Export the model
module.exports = model(DOCUMENT_NAME, userSchema);
