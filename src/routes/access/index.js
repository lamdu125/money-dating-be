'use strict'
const asyncHandler = require('../../helpers/asyncHandler')
const express = require('express')
const accessController = require('../../controllers/access.controller')
const { authentication } = require('../../auth/authUtils')
const router = express.Router()

//sign up
router.post('/user/signup', asyncHandler(accessController.signUp))
//login
router.post('/user/login', asyncHandler(accessController.login))
//login facebook
router.post('/user/login-facebook', asyncHandler(accessController.loginFacebook))
//login google
router.post('/user/login-google', asyncHandler(accessController.loginGoogle))
//forgot password
router.post('/user/forgot-password', asyncHandler(accessController.forgotPassword))
// reset password
router.post('/user/reset-password', asyncHandler(accessController.resetPassword))
//middleware authentication
router.use(authentication)
//handler access token
router.post('/user/auth', asyncHandler(accessController.handlerAccessToken))
//handler access token
router.post('/user/refresh-token', asyncHandler(accessController.handlerRefreshToken))
//logout
router.post('/user/logout', asyncHandler(accessController.logout))


module.exports = router