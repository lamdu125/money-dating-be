'use strict'
const asyncHandler = require('../../helpers/asyncHandler')
const express = require('express')
const CategoryController = require('../../controllers/category.controller')
const { authentication } = require('../../auth/authUtils')
const router = express.Router()

router.use(authentication)
//get all categories of user
router.get('/:type', asyncHandler(CategoryController.listCategories))
//update a category
router.patch('/:catId', asyncHandler(CategoryController.updateCategory))
//create a category
router.post('', asyncHandler(CategoryController.createCategory))
//delete a category
router.delete('/:catId/:replaceId?', asyncHandler(CategoryController.deleteCategory))



module.exports = router