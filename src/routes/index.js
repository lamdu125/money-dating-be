'use strict'

const express = require('express')
const router = express.Router()
const { apiKey, permission } = require('../auth/checkAuth');

//Check ApiKey
router.use(apiKey);
//Check permission 
router.use(permission('0000'));

router.use('/v1/api', require('./access'))
router.use('/v1/api/category', require('./category'))
router.use('/v1/api/wallet', require('./wallet'))
router.use('/v1/api/transaction', require('./transaction'))

module.exports = router