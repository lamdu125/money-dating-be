'use strict'
const asyncHandler = require('../../helpers/asyncHandler')
const express = require('express')
const transactionController = require('../../controllers/transaction.controller')
const { authentication } = require('../../auth/authUtils')
const router = express.Router()


router.use(authentication)
//create a transaction
router.post('', asyncHandler(transactionController.createTransaction))
//get list transaction
router.post('/list', asyncHandler(transactionController.getListTransactions))
//delete transaction
router.post('/delete', asyncHandler(transactionController.deleteTransactions))
//update transaction
router.patch('/:transactionId', asyncHandler(transactionController.updateTransaction))
//get wallet static
router.post('/wallet-static', asyncHandler(transactionController.getWalletStatic))
//get transaction static
router.post('/transaction-static', asyncHandler(transactionController.getTransactionStatic))
//get category static
router.post('/category-static', asyncHandler(transactionController.getExpenseCategoryStatic))
//export csv file
router.post('/export-csv', asyncHandler(transactionController.exportCSV))

module.exports = router