'use strict'
const asyncHandler = require('../../helpers/asyncHandler')
const express = require('express')
const walletController = require('../../controllers/wallet.controller')
const { authentication } = require('../../auth/authUtils')
const router = express.Router()

router.use(authentication)
//get all wallets of user
router.get('/', asyncHandler(walletController.listWallets))
//update a wallet
router.patch('/:walletId', asyncHandler(walletController.updateWallet))
//create a wallet
router.post('', asyncHandler(walletController.createWallet))
//delete a wallet
router.delete('/:walletId', asyncHandler(walletController.deleteWallet))

module.exports = router