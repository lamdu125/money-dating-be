'use strict'

//library
const bcrypt = require('bcrypt');
const crypto = require('crypto')
const path = require('path');
//model
const userModel = require("../models/user.model")
//utils, helper
const { BadRequestError, ConflictRequestError, AuthFailureError, ForbiddenError, NotFoundError } = require("../core/error.response");
const { createTokenPair } = require("../auth/authUtils");
const { isEmail, isPasswordValid } = require('../helpers/validation')
const { readHtmlFile } = require("../helpers/fileHandler");
//service
const KeyTokenService = require("./keytoken.service");
const { findByEmail } = require("./user.service");
const { loginWithFaceBook } = require("./facebook.service");
const { loginWithGoogle } = require("./google.service");
const ResetTokenService = require("./resettoken.service");
const mailService = require("./mailer.service");


const { FRONT_END_LOCALHOST_URI } = process.env
const RoleUser = {
    USER: 'USER',
    ADMIN: 'ADMIN'
}
const defaultCategories = require('../configs/defaultCategories')
const defaultWallets = require('../configs/defaultWallets')
class AccessService {

    static resetPassword = async ({ userId, token, password, repeatPassword }) => {
        if (!userId || !token || !password || !repeatPassword) throw new BadRequestError('Invalid request');
        if (!isPasswordValid(password)) throw new BadRequestError('Password is not valid')

        const foundToken = await ResetTokenService.findByUserId({ userId });
        if (!foundToken) throw new BadRequestError('Invalid request');

        const match = await bcrypt.compare(token, foundToken.token);
        if (!match) throw new BadRequestError('Invalid token')

        if (password !== repeatPassword) throw new BadRequestError(`Passwords don't match`)

        const delToken = await ResetTokenService.removeTokenById(foundToken._id.toString())
        if (!delToken) throw new BadRequestError('Some thing wrong');

        const hashPassword = await bcrypt.hash(password, 10);
        const user = await userModel.findByIdAndUpdate(userId, {
            password: hashPassword
        }, { new: true });
        if (!user) throw new NotFoundError('Not found user');

        const { email, name } = user;

        const privateKey = crypto.randomBytes(64).toString('hex')
        const publicKey = crypto.randomBytes(64).toString('hex');
        const tokens = await createTokenPair({ userId, email }, publicKey, privateKey)
        const newKeyStore = await KeyTokenService.createKeyToken({
            userId,
            refreshToken: tokens.refreshToken,
            publicKey,
            privateKey
        })
        if (!newKeyStore) {
            throw new BadRequestError('Cannot create ... in db');
        }

        return {
            user: {
                userId,
                name,
                email
            },
            tokens
        }
    }

    static forgotPassword = async ({ email }) => {
        const user = await findByEmail({ email });
        if (!user) throw new BadRequestError('Email is not exist')
        if (user.authProvider) throw new BadRequestError('Your email is not sign up with password')

        const token = crypto.randomBytes(16).toString('hex');
        const hashToken = await bcrypt.hash(token, 10);
        const resetToken = await ResetTokenService.createResetToken({ userId: user._id.toString(), token: hashToken, expireTime: '1d' })
        if (!resetToken) {
            throw new BadRequestError('Cannot create ... in db');
        }

        const resetPasswordUrl = `${FRONT_END_LOCALHOST_URI}/reset-password/?id=${user._id.toString()}&token=${token}`;
        const filePath = path.join(__dirname, '../email-template/forgot-password.html');
        let htmlContent = await readHtmlFile(filePath);
        htmlContent = htmlContent.replace('[username]', user.name).replace('[reset-password-link]', resetPasswordUrl);

        const messageId = await mailService.sendMail({
            to: email,
            subject: 'Reset your password',
            text: `Hello ${user._id.toString()} this is your reset password url: ${resetPasswordUrl}`,
            html: htmlContent
        })
        if (!messageId) throw new BadRequestError('Fail to send mail');
        return messageId;
    }

    static handlerAccessToken = ({ userId, email, name }) => {
        return {
            userId,
            email,
            name
        }
    }

    static handlerRefreshToken = async ({ keyStore, user, refreshToken }) => {
        const { userId, email } = user
        if (keyStore.refreshTokenUsed.includes(refreshToken)) {
            await KeyTokenService.deleteKeyByUserId(userId);
            throw new ForbiddenError('Something wrong happened, please login again');
        }
        if (keyStore.refreshToken !== refreshToken) throw new AuthFailureError('Shop is not registered');

        const foundUser = await findByEmail({ email });
        if (!foundUser) throw new AuthFailureError('Invalid User')

        const tokens = await createTokenPair({ userId, email }, keyStore.publicKey, keyStore.privateKey);

        await keyStore.updateOne(
            {
                $set: {
                    refreshToken: tokens.refreshToken
                },
                $addToSet: {
                    refreshTokenUsed: refreshToken
                }
            }
        )
        return {
            user,
            tokens
        }

    }

    static logout = async (keyStore) => {
        if (!keyStore) return
        const delKey = await KeyTokenService.removeKeyById(keyStore._id)
        if (!delKey) throw new BadRequestError('Some thing wrong');
        return delKey
    }

    static login = async ({ email, password }) => {
        if (!email || !password) throw new BadRequestError('Invalid request')
        const foundUser = await userModel.findOne({ email }).lean();
        if (!foundUser) throw new BadRequestError('email is not registered')
        if (foundUser.authProvider) throw new BadRequestError('email is not registered with password')

        const match = await bcrypt.compare(password, foundUser.password)
        if (!match) throw new BadRequestError('Password is not correct')

        const privateKey = crypto.randomBytes(64).toString('hex');
        const publicKey = crypto.randomBytes(64).toString('hex');

        const { _id: userId } = foundUser
        const tokens = await createTokenPair({ userId, email }, publicKey, privateKey);
        const newKeyStore = await KeyTokenService.createKeyToken({
            userId,
            refreshToken: tokens.refreshToken,
            publicKey,
            privateKey
        })

        if (!newKeyStore) {
            throw new BadRequestError('Cannot create ... in db');
        }

        const user = {
            userId: foundUser._id,
            email,
            name: foundUser.name
        }

        return {
            user,
            tokens
        }

    }
    static logInWithAuthProvider = async ({ authProvider, token }) => {
        let userInfo
        if (authProvider === 'facebook') {
            userInfo = await loginWithFaceBook(token);
        }
        if (authProvider === 'google') {
            userInfo = await loginWithGoogle(token);
        }

        const { name, email } = userInfo
        let userId
        const holderUser = await userModel.findOne({ email }).lean()
        if (holderUser) {
            if (holderUser.authProvider !== authProvider) {
                throw new ConflictRequestError('Email is already exist!')
            }
            userId = holderUser._id
        } else {
            const newUser = await userModel.create({ name, email, roles: [RoleUser.USER], categories: defaultCategories, authProvider, wallets: defaultWallets })
            if (!newUser) {
                throw new NotFoundError('Something went wrong')
            }
            userId = newUser._id
        }
        //create tokens
        const privateKey = crypto.randomBytes(64).toString('hex')
        const publicKey = crypto.randomBytes(64).toString('hex');
        const tokens = await createTokenPair({ userId, email }, publicKey, privateKey)
        const newKeyStore = await KeyTokenService.createKeyToken({
            userId,
            refreshToken: tokens.refreshToken,
            publicKey,
            privateKey
        })
        if (!newKeyStore) {
            throw new BadRequestError('Cannot create ... in db');
        }


        return {
            user: {
                userId,
                name,
                email
            },
            tokens
        }
    }


    static signUp = async ({ name, email, password }) => {
        if (!isEmail(email)) throw new BadRequestError('Email is not valid');
        if (!name) throw new BadRequestError('Name is empty')
        if (!isPasswordValid(password)) throw new BadRequestError('Password is not valid')

        const holderUser = await userModel.findOne({ email }).lean();
        if (holderUser) {
            throw new ConflictRequestError('Email already registered!')
        }
        const passwordHash = await bcrypt.hash(password, 10);

        const newUser = await userModel.create({ name, email, password: passwordHash, roles: [RoleUser.USER], categories: defaultCategories, wallets: defaultWallets })

        if (newUser) {
            const publicKey = crypto.randomBytes(64).toString('hex');
            const privateKey = crypto.randomBytes(64).toString('hex');


            const tokens = await createTokenPair({ userId: newUser._id, email }, publicKey, privateKey);
            console.log(`create tokens success::`, tokens);
            const keyStore = await KeyTokenService.createKeyToken({
                userId: newUser._id,
                publicKey,
                privateKey,
                refreshToken: tokens.refreshToken
            })

            if (!keyStore) {
                throw new BadRequestError('Cannot create ... in db');
            }
            const user = {
                userId: newUser._id,
                email,
                name: newUser.name
            }
            return {
                user,
                tokens
            }
        }
        return {
            code: 201,
            metadata: null
        }
    }

}

module.exports = AccessService