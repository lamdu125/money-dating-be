'use strict'

const { updateCategoryById, createCategory, deleteCategoryById, getListCatByType } = require("../models/repositories/category.repo");
const TransactionService = require("./transaction.service");


class CategoryService {
    static getCustomCategories = async ({ userId, type }) => {
        return await getListCatByType(userId, type);
    }
    static updateCategory = async ({ userId, catId, payload }) => {
        return await updateCategoryById(userId, catId, payload);
    }
    static createCategory = async ({ userId, payload }) => {
        return await createCategory(userId, payload);
    }
    static deleteCategory = async ({ userId, catId, replaceId }) => {
        if (replaceId) {
            const { success, updatedTransaction } = await TransactionService.moveTransactionsToCat(userId, catId, replaceId);
            console.log(`${success ? 'success' : 'failed'} update ${updatedTransaction} transaction`)
        } else {
            const isDeletedTransaction = await TransactionService.deleteTransactionByCategory(userId, catId);
            if (!isDeletedTransaction) throw new NotFoundError('delete transaction failed');
        }
        return await deleteCategoryById(userId, catId);
    }
}

module.exports = CategoryService