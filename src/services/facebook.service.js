const axios = require('axios')
const { BadRequestError } = require('../core/error.response')


const verifyFbAccessToken = async (token) => {
    const url = `https://graph.facebook.com/me?fields=name,email&access_token=${token}`
    try {
        const response = await axios.get(url);
        return {
            email: response.data.email,
            name: response.data.name
        }
    } catch (error) {
        throw new BadRequestError('Token is invalid', error)
    }
}

const loginWithFaceBook = async (accessToken) => {
    return user = await verifyFbAccessToken(accessToken);
}

module.exports = {
    verifyFbAccessToken,
    loginWithFaceBook
}