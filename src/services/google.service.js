const { OAuth2Client } = require('google-auth-library');
const { AuthFailureError } = require('../core/error.response');

const clientId = process.env.GOOGLE_CLIENT_ID
const clientSecret = process.env.GOOGLE_SECRET_ID
const redirectUri = process.env.GOOGLE_REDIRECT_URI

const oAuth2Client = new OAuth2Client(clientId, clientSecret, redirectUri)


const verifyGGUser = async (token) => {
    try {
        const ticket = await oAuth2Client.verifyIdToken({
            idToken: token,
        })
        const payload = ticket.getPayload()
        const email = payload['email']
        const name = payload['name']
        return {
            email,
            name
        }
    } catch (error) {
        throw new AuthFailureError('Invalid user');
    }
}

const loginWithGoogle = async (authToken) => {
    return user = await verifyGGUser(authToken);
}

module.exports = {
    verifyGGUser,
    loginWithGoogle
}