const nodemailer = require('nodemailer');
const { GOOGLE_EMAIL_NAME, GOOGLE_EMAIL_PASSWORD } = process.env

class MailService {
    constructor() {
        this.transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: GOOGLE_EMAIL_NAME,
                pass: GOOGLE_EMAIL_PASSWORD
            }
        })
    }
    async sendMail({ to, subject, text, html = '' }) {
        const mailOptions = {
            from: this.transporter.options.auth.user,
            to,
            subject,
            text,
            html
        }
        try {
            const info = await this.transporter.sendMail(mailOptions);
            console.log(`Message sent: ${info.messageId}`);
            return info.messageId;
        } catch (error) {
            console.error('Error sending email:', error);
            return null;
        }
    }
}

const mailService = new MailService()

module.exports = mailService
