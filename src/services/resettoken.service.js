'use strict'
const { convertToFutureDate } = require("../helpers/timeHandler")
const resetTokenModel = require("../models/resettoken.model")

class ResetTokenService {

    static createResetToken = async ({ userId, token, expireTime }) => {
        const expiresAt = convertToFutureDate(expireTime);
        const filter = { user: userId }
        const update = { token, expiresAt }
        const options = {
            upsert: true,
            new: true
        }
        const newToken = await resetTokenModel.findOneAndUpdate(filter, update, options);
        return token ? newToken.token : null
    }

    static findByUserId = async ({ userId, select = { token: 1 } }) => {
        return await resetTokenModel.findOne({ user: userId }).select(select).lean()
    }
    static removeTokenById = async (id) => {
        return await resetTokenModel.findByIdAndDelete(id);
    }
}

module.exports = ResetTokenService