'use strict'

const { BadRequestError, NotFoundError, ForbiddenError } = require("../core/error.response");
const { getCategoryById, getCustomCategoriesByUserId } = require("../models/repositories/category.repo");
const { createTransaction, getTransactions, deleteTransactionsByIds, getTransactionById, updateTransactionById, getTransactionByWallet, getTransactionByCategory, getTransactionsByTimePeriod, getTransactionsStaticPerDay, getCategoryStatic, getExpenseCategoryStatic, getTransactionsExport } = require("../models/repositories/transaction.repo");
const { getWalletsByUserId } = require("../models/repositories/wallet.repo");
const { getPreviousPeriod, formatDate } = require("../utils");
const { Parser } = require('json2csv')
class TransactionService {
    static create = async ({ userId, payload }) => {
        return await createTransaction(userId, payload);
    }

    static getListTransactions = async ({ userId, query }) => {
        return await getTransactions({ userId, ...query });
    }

    static deleteTransactions = async ({ transactionIds }) => {
        if (!Array.isArray(transactionIds) || transactionIds.length === 0) {
            throw new BadRequestError("Invalid transaction IDs");
        }
        return await deleteTransactionsByIds(transactionIds);
    }
    static updateTransaction = async ({ userId, transactionId, payload }) => {
        const foundTransaction = await getTransactionById(transactionId);
        if (!foundTransaction) throw new NotFoundError('Not found transaction');
        if (userId !== foundTransaction.user_id.toString()) throw new ForbiddenError("user invalid");
        const updatedTransaction = await updateTransactionById(transactionId, payload);
        const foundCat = await getCategoryById(userId, updatedTransaction.category_id);
        if (foundCat.type === 'expense' && updatedTransaction.amount > 0) {
            return await updateTransactionById(transactionId, { amount: -updatedTransaction.amount });
        }
        if (foundCat.type === 'income' && updatedTransaction.amount < 0) {
            return await updateTransactionById(transactionId, { amount: -updatedTransaction.amount });
        }
        return updatedTransaction
    }
    static deleteTransactionByWallet = async (userId, walletId) => {
        const foundTransIds = await getTransactionByWallet(userId, walletId);
        return await deleteTransactionsByIds(foundTransIds);
    }
    static deleteTransactionByCategory = async (userId, catId) => {
        const foundTransIds = await getTransactionByCategory(userId, catId);
        return await deleteTransactionsByIds(foundTransIds);
    }

    static moveTransactionsToCat = async (userId, catId, replaceId) => {
        const foundTransIds = await getTransactionByCategory(userId, catId);
        if (foundTransIds.length === 0) {
            return {
                success: true,
                updatedTransaction: 0
            }
        }
        const updatePromises = foundTransIds.map(transactionId => {
            return updateTransactionById(transactionId, { category_id: replaceId });
        })
        await Promise.all(updatePromises);

        return {
            success: true,
            updatedTransaction: foundTransIds.length
        }

    }

    static getWalletStatic = async ({ userId, walletId, startDate, endDate }) => {
        const transactions = await getTransactionsByTimePeriod(userId, walletId, startDate, endDate);

        const balance = transactions.reduce((total, transaction) => total + transaction.amount, 0);


        const totalIncome = transactions.reduce((totalIncome, transaction) => {
            if (transaction.amount >= 0) return totalIncome + transaction.amount;
            return totalIncome;
        }, 0);

        const totalExpense = transactions.reduce((totalExpense, transaction) => {
            if (transaction.amount < 0) return totalExpense + transaction.amount;
            return totalExpense;
        }, 0);

        if (!startDate || !endDate) {
            return {
                balance,
                totalExpense,
                totalIncome,
                previousBalance: undefined,
                previousTotalIncome: undefined,
                previousTotalExpense: undefined,
            };
        }

        const { previousStart, previousEnd } = getPreviousPeriod(startDate, endDate);
        const previousTransactions = await getTransactionsByTimePeriod(userId, walletId, previousStart.toISOString(), previousEnd.toISOString());

        const previousBalance = previousTransactions.reduce((total, transaction) => total + transaction.amount, 0);

        const previousTotalIncome = previousTransactions.reduce((totalIncome, transaction) => {
            if (transaction.amount >= 0) return totalIncome + transaction.amount;
            return totalIncome;
        }, 0);

        const previousTotalExpense = previousTransactions.reduce((totalExpense, transaction) => {
            if (transaction.amount < 0) return totalExpense + transaction.amount;
            return totalExpense;
        }, 0);

        return {
            balance,
            totalExpense,
            totalIncome,
            previousBalance,
            previousTotalIncome,
            previousTotalExpense,
        };
    };

    static getTransactionsStaticPerDay = async ({ userId, walletId, startDate, endDate }) => {
        return await getTransactionsStaticPerDay(userId, walletId, startDate, endDate);
    }
    static getExpenseCategoryStatic = async ({ userId, walletId, startDate, endDate }) => {
        return await getExpenseCategoryStatic(userId, walletId, startDate, endDate);
    }

    static exportCSV = async ({ userId, query }) => {
        const transactions = await getTransactionsExport({ userId, ...query });
        console.log(transactions);
        const categories = await getCustomCategoriesByUserId(userId);
        const wallets = await getWalletsByUserId(userId);

        const categoryMap = categories.reduce((map, category) => {
            map[category._id] = category.name;
            return map;
        }, {});

        const walletMap = wallets.reduce((map, wallet) => {
            map[wallet._id] = wallet.name;
            return map;
        }, {});

        const mappedTransactions = transactions.map(transaction => {
            return {
                ...transaction,
                categoryName: categoryMap[transaction.category_id],
                walletName: walletMap[transaction.wallet_id],
                formatDate: formatDate(transaction.date)
            };
        });
        const json2csvParser = new Parser({ fields: ['formatDate', 'categoryName', 'walletName', 'amount', 'note'] });
        return json2csvParser.parse(mappedTransactions);
    }
}

module.exports = TransactionService