'use strict'

const userModel = require("../models/user.model")

const findByEmail = async ({ email, select = {
    email: 1, name: 1, status: 1, roles: 1, authProvider: 1
} }) => {
    return await userModel.findOne({ email }).select(select).lean()
}
const findUserById = async (id) => {
    const user = await userModel.findById(id);
    if (!user) throw new NotFoundError('Not found user');
    return user
}

module.exports = {
    findByEmail,
    findUserById
}