const { NotFoundError } = require("../core/error.response");
const { getWalletsByUserId, updateWalletById, createWallet, deleteWalletById, getWalletById, updateWalletBalance } = require("../models/repositories/wallet.repo");
const { removeUndefinedObject, updateNestedObjectParser } = require("../utils");
const TransactionService = require("./transaction.service");

class WalletService {
    static getCustomWallets = async ({ userId }) => {
        return await getWalletsByUserId(userId);
    }
    static updateWallet = async ({ userId, walletId, payload }) => {
        const rawUpdateBody = removeUndefinedObject(payload);
        const flatUpdateBody = updateNestedObjectParser(rawUpdateBody);
        return await updateWalletById(userId, walletId, flatUpdateBody);
    }
    static createWallet = async ({ userId, payload }) => {
        return await createWallet(userId, payload);
    }
    static deleteWallet = async ({ userId, walletId }) => {
        const isDeletedTransaction = await TransactionService.deleteTransactionByWallet(userId, walletId);
        if (!isDeletedTransaction) throw new NotFoundError('delete transaction failed');
        return await deleteWalletById(userId, walletId);
    }
}

module.exports = WalletService

