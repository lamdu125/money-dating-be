'use strict'
const _ = require('lodash');
const { Types } = require('mongoose')


const getInfoData = ({ fields = [], object = {} }) => {
    return _.pick(object, fields)
}
const convertToObjectIdMongodb = id => new Types.ObjectId(id)

const updateNestedObjectParser = (obj, parentKey = '') => {
    const final = {};
    Object.keys(obj).forEach(key => {
        const fullPath = parentKey ? `${parentKey}.${key}` : key;
        if (typeof obj[key] === 'object' && !Array.isArray(obj[key])) {
            const nestedUpdates = updateNestedObjectParser(obj[key], fullPath);
            Object.assign(final, nestedUpdates);
        } else {
            final[fullPath] = obj[key];
        }
    });
    return final;
};
const removeUndefinedObject = obj => {
    Object.keys(obj).forEach(k => {
        if (obj[k] == null) {
            delete obj[k]
        }
    })
    return obj
}
//['a','b']={a:1, b: 1}
const getSelectData = (select = []) => {
    return Object.fromEntries(select.map(el => [el, 1]))
}

const getPreviousPeriod = (startDate, endDate) => {
    const start = new Date(startDate);
    const end = new Date(endDate);

    const previousEnd = new Date(start - 1);
    const previousStart = new Date(previousEnd);
    previousStart.setDate(previousStart.getDate() - (end - start) / (1000 * 60 * 60 * 24));

    return {
        previousStart,
        previousEnd,
    };
};

const formatDate = (dateInput) => {
    const date = new Date(dateInput);
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const year = String(date.getFullYear());

    return `${day}/${month}/${year}`;
}

module.exports = {
    getInfoData,
    convertToObjectIdMongodb,
    updateNestedObjectParser,
    removeUndefinedObject,
    getSelectData,
    getPreviousPeriod,
    formatDate
}